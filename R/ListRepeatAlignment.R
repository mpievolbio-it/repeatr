#' @title ListRepeatAlignment
#' @name ListRepeatAlignment
#' @description Helper function to get all repeat alignments for a list of
#' repeats against one repeat.
#' @param x \code{list} of repeats as \code{DNAStringSet}
#' @param y repeat as \code{DNAStringSet}
#' @param dist.type distance calculation method if no gendist matrix is
#' supplied [default: hamming]
#' @param dna.model a character string specifying the evolutionary model to be
#' used (see \code{ape::dist.dna}) [default: K80]
#' @param wmut weight for nucleotide point mutations [default: 1]
#' @param windel weight for unit insertions/deletions [default: 3.5]
#' @param wslippage weight for single unit duplications [default: 1.75]
#' @param exclude.pos positions of the repeat that will be excluded before
#' distance calculation [default: NULL]
#' @param post.include boolean if excluded positions should be added back for
#' the sequence alignment after distance calculation [default: FALSE]
#' @param threads number of parallel threads [default: 1]
#' @import foreach
#' @import doMC
#' @importFrom Biostrings DNAString DNAStringSet AAString AAStringSet
#' readDNAStringSet readAAStringSet writeXStringSet width subseq translate
#' @importFrom ape dist.dna
#' @seealso \code{\link[ape]{dist.dna}}
#' @references Vara C et al. (2019) PRDM9 Diveristy at Fine Geographical Scale
#' Reveals Contrasting Evolutionary Patterns and Functional Constraints in
#' Natural Populations of House Mice. \emph{Molecular Biology and Evolution},
#' \bold{36(8)}, 1686-1700.
#' @examples
#' ##load example sequence data
#' data("mousePRDM9", package="repeatR")
#' myRepPattern<-"PY"
#' myRepLength<-84
#' mousePRDM9.random<-sample(mousePRDM9, 20)
#' mousePRDM9.random.split<-repeatR::splitRepByPattern(mousePRDM9.random,
#'     myRepPattern, myRepLength)
#' ##get the repeat with max number of repeat units
#' mousePRDM9.random.split.repeat.units<-unlist(lapply(
#'     mousePRDM9.random.split$cds, length))
#' max.pos<-which(mousePRDM9.random.split.repeat.units==
#'     max(mousePRDM9.random.split.repeat.units))
#' ##select one/the longest repeat as the target sequences
#' target.repeat<-mousePRDM9.random.split$cds[[sample(max.pos,1)]]
#' ##align all repeats in the list to the target repeat
#' mousePRDM9.random.alg<-repeatR::ListRepeatAlignment(
#'     mousePRDM9.random.split$cds, target.repeat)
#' mousePRDM9.random.alg
#' ##change settings as for the RepeatAlignment function
#' @export ListRepeatAlignment
#' @author Kristian K Ullrich

ListRepeatAlignment<-function(x, y, dist.type="hamming", dna.model="K80",
    wmut=1, windel=3.5, wslippage=1.75, exclude.pos=NULL, post.include=FALSE,
    threads=1){
    #check if all repeat units have the same length
    stopifnot(unique(unlist(lapply(x, function(x) {
        unique(width(x))})))==unique(width(y)))
    x.repeat.units <- unlist(lapply(x, length))
    x.red <- x[x.repeat.units!=1]
    x.red.repeat.units <- unlist(lapply(x.red, length))
    x.ordered <- x.red[order(x.red.repeat.units, decreasing=TRUE)]
    doMC::registerDoMC(threads)
    out <- NULL
    i <- NULL
    out <- foreach::foreach(i=seq(from=1, to=length(x.ordered)),
        .combine=c) %dopar% {
        tmp.x <- x.ordered[[i]]
        tmp.x.name <- names(x.ordered)[i]
        tmp.alg <- repeatR::RepeatAlignment(y, tmp.x, dist.type=dist.type,
            dna.model=dna.model, wmut=wmut, windel=windel,
            wslippage=wslippage, exclude.pos=exclude.pos,
            post.include=post.include)
        names(tmp.alg$sequence)[2] <- tmp.x.name
        return(tmp.alg$sequence[2])
    }
    return(out)
}
